This code is hereby published under the ABAM license.

The ABAM (Anyone But Avery Magnotti) forbids the usage and distribution of this code to "Avery Magnotti" or if your primary online alias is "citrusui." This forbids any usage of this code with any projects involving "Avery Magnotti" or "citrusui" in any open-source or closed-source project. An additional exception to the ABAM exists, stating that friends of "Avery Magnotti" or "citrusui" are forbidden to use/redistribute this code to "Avery Magnotti" or "citrusui", but only when in a 5 mile radius of "Avery Magnotti" or "citrusui", or when having any discussion with him. 

If this license is revoked or changed to a new license in the future, all previous commits will still be licensed under the ABAM and still cannot be used by "Avery Magnotti" or "citrusui".

No, this license isn't a joke. It's very serious.