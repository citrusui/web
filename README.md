# web

The code that powers plusreed.com.

## License
This code is licensed under the ABAM. 
...No, that license isn't a joke. I wouldn't take it as a joke, either. 
You may see the full license by viewing the LICENSE.md file in the root of this repository.